﻿namespace YoutubeDownloader
{
	partial class Form1
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Label = new System.Windows.Forms.Label();
			this.URLLink = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.Download = new System.Windows.Forms.Button();
			this.Labelvalid = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// Label
			// 
			this.Label.AutoSize = true;
			this.Label.Font = new System.Drawing.Font("Verdana", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
			this.Label.Location = new System.Drawing.Point(120, 9);
			this.Label.Name = "Label";
			this.Label.Size = new System.Drawing.Size(291, 22);
			this.Label.TabIndex = 0;
			this.Label.Text = "Youtube downloader (mp4)";
			// 
			// URLLink
			// 
			this.URLLink.AutoSize = true;
			this.URLLink.Font = new System.Drawing.Font("Sitka Small", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.URLLink.Location = new System.Drawing.Point(8, 49);
			this.URLLink.Name = "URLLink";
			this.URLLink.Size = new System.Drawing.Size(92, 23);
			this.URLLink.TabIndex = 1;
			this.URLLink.Text = "Video Url: ";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(106, 49);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(340, 27);
			this.textBox1.TabIndex = 2;
			this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			// 
			// Download
			// 
			this.Download.Location = new System.Drawing.Point(239, 137);
			this.Download.Name = "Download";
			this.Download.Size = new System.Drawing.Size(94, 29);
			this.Download.TabIndex = 3;
			this.Download.Text = "Download";
			this.Download.UseVisualStyleBackColor = true;
			this.Download.Click += new System.EventHandler(this.Download_ClickAsync);
			// 
			// Labelvalid
			// 
			this.Labelvalid.Location = new System.Drawing.Point(106, 95);
			this.Labelvalid.Name = "Labelvalid";
			this.Labelvalid.Size = new System.Drawing.Size(340, 20);
			this.Labelvalid.TabIndex = 4;
			this.Labelvalid.Text = "-";
			this.Labelvalid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.Labelvalid.Click += new System.EventHandler(this.label1_Click_1);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(352, 137);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(94, 29);
			this.button1.TabIndex = 5;
			this.button1.Text = "Cancel";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(458, 171);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.Labelvalid);
			this.Controls.Add(this.Download);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.URLLink);
			this.Controls.Add(this.Label);
			this.Name = "Form1";
			this.Text = "Youtube downloader";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label Label;
		private System.Windows.Forms.Label URLLink;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button Download;
		private System.Windows.Forms.Label Labelvalid;
		private System.Windows.Forms.Button button1;
	}
}

