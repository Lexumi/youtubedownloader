﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using VideoLibrary;

namespace YoutubeDownloader
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void label1_Click(object sender, EventArgs e)
		{
		}

		private void Form1_Load(object sender, EventArgs e)
		{
		}

		//download
		private async void Download_ClickAsync(object sender, EventArgs e)
		{
			using (FolderBrowserDialog fbd = new FolderBrowserDialog()
			{
				Description = "Choose Download destination"
			})
			{
				if (fbd.ShowDialog() == DialogResult.OK)
				{
					Labelvalid.Text = "Procces started, please wait";
					Labelvalid.ForeColor = Color.Red;
					var youtube = YouTube.Default;
					var video = await youtube.GetVideoAsync(textBox1.Text);
					File.WriteAllBytes(fbd.SelectedPath + @"\" + video.FullName, await video.GetBytesAsync());

					Labelvalid.Text = "Download finished";
					Labelvalid.ForeColor = Color.Green;
				}
				else
				{
					MessageBox.Show("Please choose a valid Filepath", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}

		//url
		private void textBox1_TextChanged(object sender, EventArgs e)
		{
		}

		//cancel
		private void button1_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		//valid
		private void label1_Click_1(object sender, EventArgs e)
		{
		}
	}
}